<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\ApiBundle\Api;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Snc\RedisBundle\Client\Phpredis\Client as RedisClient;
use Nuvo\ApiBundle\Api\Client as ApiClient;

class AbstractClient extends ContainerAware {
	const CACHE_TTL = 600;

	protected $api;
	protected $logger;
	protected $redis;
	protected $router;

	public function __construct(ApiClient $api, Logger $logger, $redis, Router $router) {
		$this->api = $api;
		$this->logger = $logger;
		$this->redis = $redis;
		$this->router = $router;
	}

	public function __call($name, array $args) {
		$key = 'api::' . $name . serialize($args);
		if (!($data = $this->redis->get($key))) {
			list($result, $ttl) = call_user_func_array(array($this, $name . 'Cached'), $args);
			$this->redis->setex($key, ($ttl == 0 ? $ttl : self::CACHE_TTL), serialize($result));

			return $result;
		} else {
			return unserialize($data);
		}
	}
}