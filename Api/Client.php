<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\ApiBundle\Api;

use Buzz\Browser;
use Snc\RedisBundle\Client\Phpredis\Client as RedisClient;

class Client {
	const GET = 'get';
	const POST = 'post';
	const PUT = 'put';
	const PATCH = 'patch';
	const DELETE = 'delete';

	protected $redis;
	protected $browser;
	protected $params;
	protected $token = null;

	public function __construct(Browser $browser, $redis, array $params) {
		$this->browser = $browser;
		$this->redis = $redis;
		$this->params = $params;
	}

	protected function connect($force = false) {
		if ($force || !$this->token) {
			$key = $this->params['affiliate_name'] . '/api_key';
			$token = $this->redis->get($key);
			if ($token === false || $force) {
				$params = array(
					'name' => $this->params['affiliate_name'],
					'password' => $this->params['affiliate_pass']
				);
				try {
					$response = $this->browser->post('/auth', array(), http_build_query($params));
					$response = json_decode($response->getContent(), true);
					$this->token = $response['token'];
					$this->redis->setex($key, 100, $this->token);
				} catch(\Exception $e) {
					if ($e->getCode() == '403') {
						throw new \Exception('bad username/password');
					}
				}
			} else {
				$this->token = $token;
			}
		}
	}

	public function doRequest($method, $uri, array $params = array()) {
		$this->connect();

		$params['token'] = $this->token;

		try {
			switch($method) {
				case self::GET:
					$response = $this->browser->get($uri . '?' . http_build_query($params));
					break;
				case self::POST:
				case self::PUT:
				case self::PATCH:
				case self::DELETE:
					$response = $this->browser->$method($uri, array(), http_build_query($params));
					break;
				default:
					throw new \Exception('unknown method: ' . $method);
					break;
			}

			return json_decode($response->getContent(), true);
		} catch(\Exception $e) {
			if ($e->getCode() == 403) {
				$this->connect(true);
				return $this->doRequest($method, $uri, $params);
			} else {
				throw new Exception($e->getCode(), json_decode($this->browser->getLastResponse()->getContent(), true));
			}
		}
	}

	public function getToken() {
		$this->connect();

		return $this->token;
	}
}