<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\ApiBundle\Api;


class Exception extends \Exception {
	private $content;

	public function __construct($code, $content) {
		$this->code = $code;
		$this->content = $content;
	}

	public function getContent() {
		return $this->content;
	}
}