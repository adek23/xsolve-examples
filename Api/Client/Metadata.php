<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\ApiBundle\Api\Client;

use Nuvo\ApiBundle\Api\Client as ApiClient;
use Nuvo\ApiBundle\Api\AbstractClient;

class Metadata extends AbstractClient {
	public function getArtists($letter, $genre = null) {
		$params = array('letter' => $letter);
		if ($genre) {
			$params['genre'] = $genre;
		}

		try {
			return $this->api->doRequest(ApiClient::GET, '/artists', $params)['artists'];
		} catch(\Exception $e) {
			if ($e->getCode() == 404) {
				return array();
			}
		}
	}
}