<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\ModelBundle\EventListener;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;

use Nuvo\ModelBundle\Document\User;

class UserCascadeRemoveListener {
	private function getPayments(DocumentManager $dm, User $user) {
		$tmpArray = $dm->getRepository('NuvoModelBundle:Basket')->createQueryBuilder()
			->select('payments')->hydrate(false)
			->field('user')->equals($user->getId())
			->field('payments')->exists(true)
			->getQuery()->execute()->toArray();
		$tmpArray = array_column($tmpArray, 'payments');

		$payments = array();
		array_walk_recursive($tmpArray, function($item, $key) use(&$payments) {
			if ($item instanceof \MongoId) {
				$payments[] = $item;
			}
		});

		return $payments;
	}

	public function preRemove(LifecycleEventArgs $eventArgs) {
		$dm = $eventArgs->getDocumentManager(); /* @var $dm DocumentManager */
		$user = $eventArgs->getDocument();
		if ($user instanceOf User) {
			// usuwamy subskrypcje
			$dm->getRepository('NuvoModelBundle:Subscription')->createQueryBuilder()->remove()
				->field('user')->equals($user->getId())
				->getQuery()->execute();

			// usuwamy płatności
			$dm->getRepository('NuvoModelBundle:Payment')->createQueryBuilder()->remove()
				->field('id')->in($this->getPayments($dm, $user))
				->getQuery()->execute();

			// usuwamy koszyki
			$dm->getRepository('NuvoModelBundle:Basket')->createQueryBuilder()->remove()
				->field('user')->equals($user->getId())
				->getQuery()->execute();
		}
	}
}