<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\S3Bundle\Service\LockManager;

use Snc\RedisBundle\Client\Phpredis\Client as RedisClient;

/**
 * Klasa blokowania
 *
 * @package Nuvo\S3Bundle\Service\LockManager
 */
class Lock {
	private $redis;
	private $name;
	private $ttl;
	private $acquired = false;

	/**
	 * Tworzymy nowy lock
	 * @param RedisClient $redis
	 * @param $namespace nazwa
	 * @param $ttl czas życia
	 * @param bool $waitForLock czy blokować inne locki
	 */
	public function __construct(RedisClient $redis, $namespace, $ttl, $waitForLock = true) {
		$this->redis = $redis;
		$this->name = 'LOCK.' . $namespace;
		$this->ttl = $ttl;

		for ($i = 0; $i < $this->ttl; $i++) {
			$timeout = time() + $this->ttl;

			if ($this->redis->setnx($this->name, $timeout)) {
				$this->redis->expire($this->name, $this->ttl);
				$this->acquired = true;
				return;
			} else {
				if (!$waitForLock) {
					break;
				}
				$currentTimeout = $this->redis->get($this->name);
				if ($currentTimeout < time()) {
					if ($currentTimeout == $this->redis->getSet($this->name, $timeout)) {
						$this->acquired = true;
						return;
					}
				}
			}
			sleep(1);
		}

		throw new Exception('Cannot get: ' . $this->name);
	}

	public function __destruct() {
		$this->release();
	}

	/**
	 * zwolnienie locka
	 */
	public function release() {
		if (!$this->acquired) {
			return;
		}

		$this->redis->del($this->name);
		$this->acquired = false;
	}
}