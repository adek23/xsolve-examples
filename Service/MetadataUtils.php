<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\RestBundle\Service;


use Doctrine\ODM\MongoDB\DocumentManager;
use Nuvo\ModelBundle\Document\Album;
use Nuvo\ModelBundle\Document\Track;
use Symfony\Component\Security\Core\SecurityContext;

class MetadataUtils {
	const PRICELIST_KEY = 'pricelist::';
	const DISCOUNTS_KEY = 'discounts::';
	const RBT_KEY = 'rbt::';
	const TTL = 3600;

	protected $context;
	protected $dm;
	protected $affiliateId;
	protected $redis;
	protected $redisData = array();

	public function __construct(SecurityContext $context, DocumentManager $dm, $redis) {
		$this->context = $context;
		$this->dm = $dm;
		$this->redis = $redis;
	}

	public function setAffiliateId($affiliateId) {
		$this->affiliateId = $affiliateId;

		return $this;
	}

	protected function getAffiliateId() {
		if (!$this->affiliateId) {
			$this->affiliateId = $this->context->getToken()->getUser()->getId();
		}

		return $this->affiliateId;
	}

	public function getPrice($object) {
		$price = 0;

		$category = $object->getCurrentPriceCategory();
		if ($category) {
			$price = $this->getCurrentPriceList()->getPrice($category->getName());
			if ($price) {
				$discounts = $this->getCurrentDiscounts();
				if ($object instanceof Album) {
					if (isset($discounts['albums'][$object->getId()])) {
						$price -= $price * $discounts['albums'][$object->getId()] / 100;
					}
				} elseif ($object instanceof Track) {
					if (isset($discounts['tracks'][$object->getId()])) {
						$price -= $price * $discounts['tracks'][$object->getId()] / 100;
					}
				}
			}
		}

		return $price;
	}

	public function getRbt($object) {
		if ($object instanceof Track) {
			$rbt = $this->getCurrentRbt();
			if (isset($rbt[$object->getId()])) {
				return $rbt[$object->getId()];
			}
		}

		return '';
	}

	protected function redisCache($key, \Closure $closure) {
		if (array_key_exists($this->getAffiliateId(), $this->redisData)) {
			return $this->redisData[$key];
		}

		$data = $this->redis->get($key);
		if ($data === false) {
			$data = $closure();
			$this->redis->set($key, serialize($data), self::TTL);
		} else {
			$data = unserialize($data);
		}

		$this->redisData[$key] = $data;

		return $data;
	}

	public function getCurrentDiscounts() {
		return $this->redisCache(self::DISCOUNTS_KEY . $this->getAffiliateId(), function() {
			$discounts = array('albums' => array(), 'tracks' => array());
			$cursor = $this->dm->getRepository('NuvoModelBundle:Discount')->getCurrentGroupDiscounts($this->getAffiliateId());
			foreach($cursor as $discount) {
				$arr = $discount->getDiscounts();
				$val = $discount->getValue();
				foreach(array('albums', 'tracks') as $type) {
					foreach($arr[$type] as $id) {
						if (isset($discounts[$type][$id])) {
							if ($discounts[$type][$id] < $val) {
								$discounts[$type][$id] = $val;
							}
						} else {
							$discounts[$type][$id] = $val;
						}
					}
				}
			}

			return $discounts;
		});
	}

	protected function getCurrentPriceList() {
		return $this->redisCache(self::PRICELIST_KEY . $this->getAffiliateId(), function() {
			return $this->dm->getRepository('NuvoModelBundle:PriceList')->getCurrentPriceList($this->getAffiliateId());
		});
	}

	protected function getCurrentRbt() {
		return $this->redisCache(self::RBT_KEY . $this->getAffiliateId(), function() {
			$data = array();

			$cursor = $this->dm->getRepository('NuvoModelBundle:Rbt')->getCurrent($this->getAffiliateId());
			foreach($cursor as $rbt) {
				foreach($rbt->getTracks() as $track) {
					if (!isset($data[$track->getId()])) {
						$data[$track->getId()] = $rbt->getDescription();
					}
				}
			}

			return $data;
		});
	}
} 