<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\ModelBundle\Service\MongoDb;

use Doctrine\ODM\MongoDB\DocumentManager;

class Distinct {
	/**
	 * @var DocumentManager $dm
	 */
	protected $dm;

	protected $db;

	protected $query;

	public function __construct(DocumentManager $dm) {
		$this->dm = $dm;
	}

	public function getRepository($repo) {
		$this->query = array('distinct' => $this->dm->getClassMetadata($repo)->getCollection());
		$this->db = $this->dm->getDocumentDatabase($repo);

		return $this;
	}

	public function field($field) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['key'] = $field;

		return $this;
	}

	public function query(array $query) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['query'] = $query;

		return $this;
	}

	public function execute() {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$res = $this->db->command($this->query);
		if ($res['ok'] == 0) {
			throw new \Doctrine\ODM\MongoDB\MongoDBException($res['errmsg']);
		}

		return $res['values'];
	}
}