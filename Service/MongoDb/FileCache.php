<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\ModelBundle\Service\MongoDb;

use Doctrine\MongoDB\Connection;

class FileCache {
	/**
	 * @var \MongoClient $mongo
	 */
	protected $mongo;

	/**
	 * @var \MongoGridFS $grid
	 */
	protected $grid;

	protected $defaultTtl;

	public function __construct(Connection $doctrineConnection, $defaultTtl = 300) {
		$this->defaultTtl = $defaultTtl;

		$doctrineConnection->connect();
		$db = $doctrineConnection->getMongoClient()->selectDb($doctrineConnection->getConfiguration()->getDefaultDB());
		$this->grid = $db->getGridFS('FileCache');
		$this->grid->ensureIndex(array('metadata.ttl' => 1), array('background' => true));
	}

	public function invalidateOld() {
		// usuwamy stare rekordy
		$cursor = $this->grid->find(array('metadata.ttl' => array('$lt' => time())), array('metadata.downloaded'));
		$fsIds = array(); $basketIds = array();
		foreach($cursor as $row) {
			if ($row->file['metadata']['downloaded'] > 0) {
				$basketIds[] = $row->file['_id'];
			}
			$fsIds[] = $row->file['_id'];
		}
		$this->grid->remove(array('_id' => array('$in' => $fsIds)));

		return $basketIds;
	}
}