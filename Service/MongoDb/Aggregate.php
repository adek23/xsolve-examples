<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\ModelBundle\Service\MongoDb;

use Doctrine\ODM\MongoDB\DocumentManager;

class Aggregate {
	/**
	 * @var DocumentManager $dm
	 */
	protected $dm;

	protected $db;

	protected $query;

	public function __construct(DocumentManager $dm) {
		$this->dm = $dm;
	}

	public function getRepository($repo) {
		$this->query = array('aggregate' => $this->dm->getClassMetadata($repo)->getCollection());
		$this->db = $this->dm->getDocumentDatabase($repo);

		return $this;
	}

	public function project(array $fields) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['pipeline'][] = array('$project' => $fields);

		return $this;
	}

	public function match(array $query) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['pipeline'][] = array('$match' => $query);

		return $this;
	}

	public function limit($count) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['pipeline'][] = array('$limit' => $count);

		return $this;
	}

	public function skip($count) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['pipeline'][] = array('$skip' => $count);

		return $this;
	}

	public function unwind($field) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['pipeline'][] = array('$unwind' => $field);

		return $this;
	}

	public function group(array $group) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['pipeline'][] = array('$group' => $group);

		return $this;
	}

	public function sort(array $sortSpec) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['pipeline'][] = array('$sort' => $sortSpec);

		return $this;
	}

	public function geoNear(array $geoSpec) {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}

		$this->query['pipeline'][] = array('$geoNear' => $geoSpec);

		return $this;
	}

	public function execute() {
		if (!$this->db) {
			throw new \Exception('specify repository first!');
		}
		if (!isset($this->query['pipeline'])) {
			throw new \Exception('specify pipeline operators first!');
		}

		$res = $this->db->command($this->query);
		if ($res['ok'] == 0) {
			throw new \Doctrine\ODM\MongoDB\MongoDBException($res['errmsg']);
		}

		return $res['result'];
	}
}