<?php
/**
 * @author      Adrian Zurek <adek.nuvo@gmail.com>
 */

namespace Nuvo\S3Bundle\Service;

use Snc\RedisBundle\Client\Phpredis\Client as RedisClient;

/**
 * Class LockManager
 * @package Nuvo\S3Bundle\Service
 *
 * Serwis służy udostępnianiu mechanizmu blokowania
 */
class LockManager {
	protected $redis;
	protected $params;

	public function __construct(RedisClient $redis, array $params) {
		$this->redis = $redis;
		$this->params = $params;

		$this->redis->select($this->params['database']);
	}

	/**
	 * Ustanowienie locka o podanej nazwie (jeśli już istnieje taki lock, to mwetoda będzie czekała na zwolnienie)
	 *
	 * @param $namespace nazwa locka
	 * @param null $ttl czas życia
	 * @return LockManager\Lock
	 */
	public function acquire($namespace, $ttl = null) {
		if (!$ttl) {
			$ttl = $this->params['default_ttl'];
		}

		return new LockManager\Lock($this->redis, $namespace, $ttl);
	}

	/**
	 * Ustanowienie locka o podanej nazwie  (lock nie będzie blokował innych)
	 * @param $namespace nazwa locka
	 * @param null $ttl czas życia
	 * @return LockManager\Lock
	 */
	public function acquireNonBlocking($namespace, $ttl = null) {
		if (!$ttl) {
			$ttl = $this->params['default_ttl'];
		}

		return new LockManager\Lock($this->redis, $namespace, $ttl, false);
	}
}